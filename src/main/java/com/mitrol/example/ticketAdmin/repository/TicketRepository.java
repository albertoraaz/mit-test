package com.mitrol.example.ticketAdmin.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mitrol.example.ticketAdmin.entity.Ticket;

/**
 * The Interface TicketRepository.
 */
@Repository
public interface TicketRepository extends JpaRepository<Ticket, Long> {
}
