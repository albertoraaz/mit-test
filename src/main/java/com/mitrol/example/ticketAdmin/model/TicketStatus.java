package com.mitrol.example.ticketAdmin.model;


/**
 * The Enum TicketStatus.
 */
public enum TicketStatus {

    /** The pending. */
    PENDING,
    
    /** The working. */
    WORKING,
    
    /** The done. */
    DONE;

}
