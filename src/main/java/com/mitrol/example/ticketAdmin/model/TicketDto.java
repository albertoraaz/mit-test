package com.mitrol.example.ticketAdmin.model;


/**
 * The Class TicketDto.
 */
public class TicketDto {

	/** The id. */
	private long id;
	
	/** The name. */
	private String name;
	
	/** The status. */
	private TicketStatus status = TicketStatus.PENDING;

	/**
	 * Instantiates a new ticket dto.
	 */
	public TicketDto() {
	}

	/**
	 * Instantiates a new ticket dto.
	 *
	 * @param id the id
	 * @param name the name
	 * @param status the status
	 */
	public TicketDto(long id, String name, TicketStatus status) {
		this.id = id;
		this.name = name;
		this.status = status;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public TicketStatus getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(TicketStatus status) {
		this.status = status;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "TicketDto [id=" + id + ", name=" + name + ", status=" + status + "]";
	}

}
