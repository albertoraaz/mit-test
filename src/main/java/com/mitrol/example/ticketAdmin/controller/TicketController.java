package com.mitrol.example.ticketAdmin.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mitrol.example.ticketAdmin.entity.Ticket;
import com.mitrol.example.ticketAdmin.model.TicketDto;
import com.mitrol.example.ticketAdmin.service.TicketService;

/**
 * The Class TicketController.
 */
@RestController
public class TicketController {

	/** The service. */
	@Autowired
	private TicketService service;

	/** The model mapper. */
	ModelMapper modelMapper = new ModelMapper();

	/**
	 * Find all.
	 *
	 * @return the list
	 */
	@GetMapping("/ticket")
	public List<TicketDto> findAll() {
		return service.findAll();
	}

	/**
	 * Creates the.
	 *
	 * @param ticket the ticket
	 * @return the ticket dto
	 */
	@PostMapping("/ticket")
	public TicketDto create(@RequestBody TicketDto ticket) {
		return service.create(ticket);
	}

	/**
	 * Gets the ticket by id.
	 *
	 * @param id the id
	 * @return the ticket by id
	 * @throws Exception the exception
	 */
	@GetMapping("/ticket/{id}")
	public ResponseEntity<TicketDto> getTicketById(@PathVariable(value = "id") Long id) throws Exception {
		TicketDto ticket = new TicketDto();
		ticket = service.findById(id);

		if (id == ticket.getId()) {
			return ResponseEntity.ok().body(ticket);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	/**
	 * Update ticket.
	 *
	 * @param id        the id
	 * @param ticketDto the ticket dto
	 * @return the response entity
	 * @throws Exception the exception
	 */
	@PutMapping("/ticket/{id}")
	public void updateEmployee(@PathVariable(value = "id") long id, @Valid @RequestBody TicketDto ticketDto)
			throws Exception {

		TicketDto ticketUpdated = new TicketDto();
		ticketUpdated = service.findById(id);

		ticketUpdated.setName(ticketDto.getName());
		ticketUpdated.setStatus(ticketDto.getStatus());

		Ticket entity = convertToEntity(ticketUpdated);

		service.update(entity);

	}

	/**
	 * Delete employee.
	 *
	 * @param id the id
	 * @return the map
	 * @throws Exception the exception
	 */
	@DeleteMapping("/ticket/{id}")
	public Map<String, Boolean> deleteTicket(@PathVariable(value = "id") long id) throws Exception {

		TicketDto ticketUpdated = new TicketDto();
		ticketUpdated = service.findById(id);

		Map<String, Boolean> map = new HashMap<String, Boolean>();

		if (id == ticketUpdated.getId() && "DONE".equals(ticketUpdated.getStatus().name())) {

			Ticket entity = convertToEntity(ticketUpdated);
			service.delete(entity);

			map.put("deleted", true);

		} else {
			map.put("deleted", false);
		}

		return map;

	}

	/**
	 * Gets the ticket not finished.
	 *
	 * @return the ticket not finished
	 * @throws Exception the exception
	 */
	@GetMapping("/ticket/notFinished")
	public List<TicketDto> getTicketNotFinished() throws Exception {

		List<TicketDto> list = new ArrayList<TicketDto>();

		list = service.findAll();

		List<TicketDto> filteredList = new ArrayList<TicketDto>();

		for (TicketDto item : list) {

			if (!("DONE".equals(item.getStatus().name()))) {

				filteredList.add(item);

			}
		}
		return filteredList;

	}

	/**
	 * Convert to entity.
	 *
	 * @param ticketDto the ticket dto
	 * @return the ticket
	 * @throws ParseException the parse exception
	 */
	private Ticket convertToEntity(TicketDto ticketDto) throws ParseException {
		Ticket ticket = modelMapper.map(ticketDto, Ticket.class);
		return ticket;
	}

}
