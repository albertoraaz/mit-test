package com.mitrol.example.ticketAdmin.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.mitrol.example.ticketAdmin.model.TicketStatus;

/**
 * The Class Ticket.
 */
@Entity
@Table(name = "ticket")
public class Ticket {

	/** The id. */
	private long id;

	/** The name. */
	private String name;

	/** The status. */
	private TicketStatus status;

	/**
	 * Instantiates a new ticket.
	 */
	public Ticket() {

	}

	public Ticket(long id, String name, TicketStatus status) {
		this.id = id;
		this.name = name;
		this.status = status;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "name", nullable = false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "status", nullable = false)
	public TicketStatus getStatus() {
		return status;
	}

	public void setStatus(TicketStatus status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Ticket [id=" + id + ", name=" + name + ", status=" + status + "]";
	}

}
