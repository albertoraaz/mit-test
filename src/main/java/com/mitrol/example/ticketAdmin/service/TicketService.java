package com.mitrol.example.ticketAdmin.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitrol.example.ticketAdmin.entity.Ticket;
import com.mitrol.example.ticketAdmin.model.TicketDto;
import com.mitrol.example.ticketAdmin.repository.TicketRepository;

/**
 * The Class TicketService.
 */
@Service
public class TicketService {

	/** The repository. */
	@Autowired
	private TicketRepository repository;

	/**
	 * To dto.
	 *
	 * @param entity the entity
	 * @return the ticket dto
	 */
	private TicketDto toDto(Ticket entity) {
		TicketDto dto = new TicketDto();
		dto.setId(entity.getId());
		dto.setName(entity.getName());
		dto.setStatus(entity.getStatus());
		return dto;
	}

	/**
	 * To entity.
	 *
	 * @param dto the dto
	 * @return the ticket
	 */
	private Ticket toEntity(TicketDto dto) {

		Ticket entity = new Ticket();
		entity.setName(dto.getName());
		entity.setStatus(dto.getStatus());
		return entity;
	}

	/**
	 * Find all.
	 *
	 * @return the list
	 */
	public List<TicketDto> findAll() {
		List<Ticket> entities = repository.findAll();
		return entities.stream().map(this::toDto).collect(Collectors.toList());
	}

	/**
	 * Creates the.
	 *
	 * @param ticket the ticket
	 * @return the ticket dto
	 */
	public TicketDto create(TicketDto ticket) {
		Ticket entity = toEntity(ticket);
		repository.save(entity);
		return toDto(entity);
	}

	/**
	 * Find by id.
	 *
	 * @param id the id
	 * @return the ticket dto
	 * @throws Exception the exception
	 */
	public TicketDto findById(long id) throws Exception {
		Ticket entity = repository.findById(id).orElseThrow(() -> new Exception("Not Found"));
		return toDto(entity);
	}

	public Ticket update(Ticket ticket) {
		return repository.save(ticket);
	}

	public void delete(Ticket entity) {
		repository.delete(entity);

	}

}
